﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brainfuck
{
    public class MicroStopwatch : Stopwatch
    {
        readonly double _microSecPerTick = 1000000D / Stopwatch.Frequency;

        public MicroStopwatch()
        {
            if (!Stopwatch.IsHighResolution)
            {
                throw new Exception("On this system the high-resolution performance counter is not available");
            }
        }

        public long ElapsedMicroseconds
        {
            get
            {
                return (long)(ElapsedTicks * _microSecPerTick);
            }
        }
    }
}
