﻿using Brainfuck.Implementations;
using Brainfuck.Implementations.WithLoopsOptimizations;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Brainfuck
{
    public partial class MainForm : Form
    {
        Dictionary<string, Type> runtimes = new Dictionary<string, Type>
        {
            { "Simple interpreter", typeof(SimpleInterpreter) },
            { "Interpreter with jump tables", typeof(InterpreterWithJumpTable) },
            { "Interpreter with JT & opcodes optimization (auto repeat)", typeof(InterpreterWithJTAndInstantRepeat) },
            { "Interpreter with additional loop optimizations", typeof(InterpreterWithLoopOptimizations) },
            { "JIT.NET simple", typeof(JitNetSimple) },
            { "JIT.NET with opcodes optimization", typeof(JitNetWithRepeatsCount) },
            { "JIT.NET with additional loop optimizations", typeof(JitNetWithRepeatsCountAndLoops) },
        };

        Dictionary<string, string> codes = new Dictionary<string, string>();

        ConcurrentQueue<string> outputTexts = new ConcurrentQueue<string>();

        public MainForm()
        {
            InitializeComponent();

            comboBoxRuntime.Items.AddRange(runtimes.Select(r => r.Key).ToArray());
            comboBoxRuntime.SelectedIndex = 0;

            {
                var executingAssembly = System.Reflection.Assembly.GetExecutingAssembly();
                var names = executingAssembly.GetManifestResourceNames()
                    .Where(name => name.Contains("BrainfuckExamples"))
                    .ToArray();

                foreach (var name in names)
                {
                    using (Stream reader = executingAssembly.GetManifestResourceStream(name))
                    {
                        var buffer = new byte[reader.Length];
                        reader.Read(buffer, 0, buffer.Length);

                        var splitted = name.Split('.');

                        codes.Add(splitted[splitted.Length - 2], Encoding.UTF8.GetString(buffer));
                    }
                }

                comboBoxCodes.Items.AddRange(codes.Select(c => c.Key).ToArray());
                comboBoxCodes.SelectedIndex = 0;
            }

            new Thread(() =>
            {
                while (true)
                {
                    var text = new StringBuilder();

                    while (outputTexts.TryDequeue(out string tmp)) 
                    {
                        text.Append(tmp);
                    }

                    if (text.Length > 0)
                    {
                        text = text.Replace(Environment.NewLine, "\n");
                        text = text.Replace("\n", Environment.NewLine);

                        textBoxOutput.Invoke(new Action(() =>
                        {
                            textBoxOutput.AppendText(text.ToString());
                        }));
                    }

                    Thread.Sleep(1000 / 60);
                }
            }).Start();

            FormClosing += (s, e) => Environment.Exit(0);
        }

        void OutputWrite(string text)
        {
            outputTexts.Enqueue(text);
        }

        Task<char> ReadInput()
        {
            var tcs = new TaskCompletionSource<char>();

            new Thread(() =>
            {
                void eventHandler(object sender, KeyPressEventArgs e)
                {
                    e.Handled = true;

                    textBoxInput.Invoke(new Action(() =>
                    {
                        textBoxInput.Enabled = false;
                        textBoxInput.KeyPress -= eventHandler;

                        char result = e.KeyChar;
                        if (result == '\r') result = '\n';

                        tcs.SetResult(result);
                    }));
                }

                textBoxInput.Invoke(new Action(() =>
                {
                    textBoxInput.KeyPress += eventHandler;
                    textBoxInput.Enabled = true;
                    textBoxInput.Focus();
                }));
            }).Start();

            return tcs.Task;
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            textBoxCode.Enabled = false;
            groupBoxSettings.Enabled = false;
            buttonStart.Enabled = false;
            comboBoxCodes.Enabled = false;

            var runtimeName = comboBoxRuntime.SelectedItem as string;

            textBoxOutput.Text = "";

            new Thread(async () =>
            {
                var runtime = Activator.CreateInstance(runtimes[runtimeName]) as IBrainfuckRuntime;

                runtime.SetOptions(new RuntimeOptions
                {
                    Verbose = checkBoxVerbose.Checked,
                    MemoryCells = Decimal.ToInt32(numericUpDownMemoryCells.Value),
                    OutputWrite = OutputWrite,
                    ReadInput = ReadInput
                });

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();

                await runtime.Execute(textBoxCode.Text);

                var stats = runtime.GetRuntimeStatistics();

                this.Invoke(new Action(() =>
                {
                    labelExecutionTime.Text = $"{stats.ExecutionTime:n0} μs ({(Math.Ceiling((double)stats.ExecutionTime / 1000D)):n0} ms)";

                    if (checkBoxVerbose.Checked)
                    {
                        labelStatsDot.Text = stats.DotOps.ToString("N0");
                        labelStatsComma.Text = stats.CommaOps.ToString("N0");
                        labelStatsPlus.Text = stats.PlusOps.ToString("N0");
                        labelStatsMinus.Text = stats.MinusOps.ToString("N0");
                        labelStatsOpenAngleBracket.Text = stats.OpenAngleBracketOps.ToString("N0");
                        labelStatsCloseAngleBracket.Text = stats.CloseAngleBracketOps.ToString("N0");
                        labelStatsOpenSquareBracket.Text = stats.OpenSquareBracketOps.ToString("N0");
                        labelStatsCloseSquareBracket.Text = stats.CloseSquareBracketOps.ToString("N0");
                        labelStatsTotal.Text = stats.TotalOps.ToString("N0");
                    }
                    else
                    {
                        labelStatsDot.Text = "-";
                        labelStatsComma.Text = "-";
                        labelStatsPlus.Text = "-";
                        labelStatsMinus.Text = "-";
                        labelStatsOpenAngleBracket.Text = "-";
                        labelStatsCloseAngleBracket.Text = "-";
                        labelStatsOpenSquareBracket.Text = "-";
                        labelStatsCloseSquareBracket.Text = "-";
                        labelStatsTotal.Text = "-";
                    }

                    textBoxCode.Enabled = true;
                    groupBoxSettings.Enabled = true;
                    buttonStart.Enabled = true;
                    comboBoxCodes.Enabled = true;
                }));
            }).Start();
        }

        private void comboBoxCodes_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBoxCode.Text = codes[comboBoxCodes.SelectedItem as string];
        }
    }
}
