﻿namespace Brainfuck
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelConsole = new System.Windows.Forms.TableLayoutPanel();
            this.groupBoxOutput = new System.Windows.Forms.GroupBox();
            this.textBoxOutput = new System.Windows.Forms.TextBox();
            this.textBoxInput = new System.Windows.Forms.TextBox();
            this.tableLayoutPanelSettings = new System.Windows.Forms.TableLayoutPanel();
            this.groupBoxProgram = new System.Windows.Forms.GroupBox();
            this.textBoxCode = new System.Windows.Forms.TextBox();
            this.groupBoxSettings = new System.Windows.Forms.GroupBox();
            this.comboBoxRuntime = new System.Windows.Forms.ComboBox();
            this.numericUpDownMemoryCells = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBoxVerbose = new System.Windows.Forms.CheckBox();
            this.groupBoxStatistics = new System.Windows.Forms.GroupBox();
            this.labelStatsTotal = new System.Windows.Forms.Label();
            this.labelStatsCloseSquareBracket = new System.Windows.Forms.Label();
            this.labelStatsOpenSquareBracket = new System.Windows.Forms.Label();
            this.labelStatsCloseAngleBracket = new System.Windows.Forms.Label();
            this.labelStatsOpenAngleBracket = new System.Windows.Forms.Label();
            this.labelStatsMinus = new System.Windows.Forms.Label();
            this.labelStatsPlus = new System.Windows.Forms.Label();
            this.labelStatsComma = new System.Windows.Forms.Label();
            this.labelStatsDot = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelExecutionTime = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.flowLayoutPanelController = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonStart = new System.Windows.Forms.Button();
            this.comboBoxCodes = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanelCode = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelMain.SuspendLayout();
            this.tableLayoutPanelConsole.SuspendLayout();
            this.groupBoxOutput.SuspendLayout();
            this.tableLayoutPanelSettings.SuspendLayout();
            this.groupBoxProgram.SuspendLayout();
            this.groupBoxSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMemoryCells)).BeginInit();
            this.groupBoxStatistics.SuspendLayout();
            this.flowLayoutPanelController.SuspendLayout();
            this.tableLayoutPanelCode.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.ColumnCount = 2;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutPanelConsole, 1, 0);
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutPanelSettings, 0, 0);
            this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 1;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(1284, 661);
            this.tableLayoutPanelMain.TabIndex = 0;
            // 
            // tableLayoutPanelConsole
            // 
            this.tableLayoutPanelConsole.ColumnCount = 1;
            this.tableLayoutPanelConsole.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelConsole.Controls.Add(this.groupBoxOutput, 0, 0);
            this.tableLayoutPanelConsole.Controls.Add(this.textBoxInput, 0, 1);
            this.tableLayoutPanelConsole.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelConsole.Location = new System.Drawing.Point(334, 0);
            this.tableLayoutPanelConsole.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanelConsole.MinimumSize = new System.Drawing.Size(950, 0);
            this.tableLayoutPanelConsole.Name = "tableLayoutPanelConsole";
            this.tableLayoutPanelConsole.RowCount = 2;
            this.tableLayoutPanelConsole.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelConsole.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanelConsole.Size = new System.Drawing.Size(950, 661);
            this.tableLayoutPanelConsole.TabIndex = 1;
            // 
            // groupBoxOutput
            // 
            this.groupBoxOutput.Controls.Add(this.textBoxOutput);
            this.groupBoxOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxOutput.Location = new System.Drawing.Point(3, 3);
            this.groupBoxOutput.Name = "groupBoxOutput";
            this.groupBoxOutput.Padding = new System.Windows.Forms.Padding(8);
            this.groupBoxOutput.Size = new System.Drawing.Size(944, 628);
            this.groupBoxOutput.TabIndex = 0;
            this.groupBoxOutput.TabStop = false;
            this.groupBoxOutput.Text = "Output";
            // 
            // textBoxOutput
            // 
            this.textBoxOutput.BackColor = System.Drawing.SystemColors.Control;
            this.textBoxOutput.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxOutput.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.textBoxOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxOutput.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxOutput.Location = new System.Drawing.Point(8, 21);
            this.textBoxOutput.Multiline = true;
            this.textBoxOutput.Name = "textBoxOutput";
            this.textBoxOutput.ReadOnly = true;
            this.textBoxOutput.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxOutput.Size = new System.Drawing.Size(928, 599);
            this.textBoxOutput.TabIndex = 0;
            this.textBoxOutput.TabStop = false;
            this.textBoxOutput.WordWrap = false;
            // 
            // textBoxInput
            // 
            this.textBoxInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxInput.Enabled = false;
            this.textBoxInput.Location = new System.Drawing.Point(3, 637);
            this.textBoxInput.Name = "textBoxInput";
            this.textBoxInput.Size = new System.Drawing.Size(944, 20);
            this.textBoxInput.TabIndex = 1;
            // 
            // tableLayoutPanelSettings
            // 
            this.tableLayoutPanelSettings.ColumnCount = 1;
            this.tableLayoutPanelSettings.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelSettings.Controls.Add(this.groupBoxProgram, 0, 0);
            this.tableLayoutPanelSettings.Controls.Add(this.groupBoxSettings, 0, 1);
            this.tableLayoutPanelSettings.Controls.Add(this.groupBoxStatistics, 0, 2);
            this.tableLayoutPanelSettings.Controls.Add(this.flowLayoutPanelController, 0, 3);
            this.tableLayoutPanelSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelSettings.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelSettings.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanelSettings.Name = "tableLayoutPanelSettings";
            this.tableLayoutPanelSettings.RowCount = 4;
            this.tableLayoutPanelSettings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelSettings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.tableLayoutPanelSettings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 191F));
            this.tableLayoutPanelSettings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.tableLayoutPanelSettings.Size = new System.Drawing.Size(334, 661);
            this.tableLayoutPanelSettings.TabIndex = 2;
            // 
            // groupBoxProgram
            // 
            this.groupBoxProgram.Controls.Add(this.tableLayoutPanelCode);
            this.groupBoxProgram.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxProgram.Location = new System.Drawing.Point(3, 3);
            this.groupBoxProgram.MinimumSize = new System.Drawing.Size(0, 300);
            this.groupBoxProgram.Name = "groupBoxProgram";
            this.groupBoxProgram.Padding = new System.Windows.Forms.Padding(8);
            this.groupBoxProgram.Size = new System.Drawing.Size(328, 325);
            this.groupBoxProgram.TabIndex = 0;
            this.groupBoxProgram.TabStop = false;
            this.groupBoxProgram.Text = "Program";
            // 
            // textBoxCode
            // 
            this.textBoxCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxCode.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxCode.Location = new System.Drawing.Point(3, 30);
            this.textBoxCode.Multiline = true;
            this.textBoxCode.Name = "textBoxCode";
            this.textBoxCode.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxCode.Size = new System.Drawing.Size(306, 263);
            this.textBoxCode.TabIndex = 0;
            this.textBoxCode.WordWrap = false;
            // 
            // groupBoxSettings
            // 
            this.groupBoxSettings.Controls.Add(this.comboBoxRuntime);
            this.groupBoxSettings.Controls.Add(this.numericUpDownMemoryCells);
            this.groupBoxSettings.Controls.Add(this.label1);
            this.groupBoxSettings.Controls.Add(this.checkBoxVerbose);
            this.groupBoxSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxSettings.Location = new System.Drawing.Point(3, 334);
            this.groupBoxSettings.Name = "groupBoxSettings";
            this.groupBoxSettings.Padding = new System.Windows.Forms.Padding(8);
            this.groupBoxSettings.Size = new System.Drawing.Size(328, 79);
            this.groupBoxSettings.TabIndex = 1;
            this.groupBoxSettings.TabStop = false;
            this.groupBoxSettings.Text = "Settings";
            // 
            // comboBoxRuntime
            // 
            this.comboBoxRuntime.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxRuntime.FormattingEnabled = true;
            this.comboBoxRuntime.Location = new System.Drawing.Point(11, 49);
            this.comboBoxRuntime.Name = "comboBoxRuntime";
            this.comboBoxRuntime.Size = new System.Drawing.Size(306, 21);
            this.comboBoxRuntime.TabIndex = 5;
            // 
            // numericUpDownMemoryCells
            // 
            this.numericUpDownMemoryCells.Location = new System.Drawing.Point(210, 23);
            this.numericUpDownMemoryCells.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDownMemoryCells.Name = "numericUpDownMemoryCells";
            this.numericUpDownMemoryCells.Size = new System.Drawing.Size(107, 20);
            this.numericUpDownMemoryCells.TabIndex = 4;
            this.numericUpDownMemoryCells.Value = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(133, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Memory cells:";
            // 
            // checkBoxVerbose
            // 
            this.checkBoxVerbose.AutoSize = true;
            this.checkBoxVerbose.Location = new System.Drawing.Point(11, 24);
            this.checkBoxVerbose.Name = "checkBoxVerbose";
            this.checkBoxVerbose.Size = new System.Drawing.Size(65, 17);
            this.checkBoxVerbose.TabIndex = 0;
            this.checkBoxVerbose.Text = "Verbose";
            this.checkBoxVerbose.UseVisualStyleBackColor = true;
            // 
            // groupBoxStatistics
            // 
            this.groupBoxStatistics.Controls.Add(this.labelStatsTotal);
            this.groupBoxStatistics.Controls.Add(this.labelStatsCloseSquareBracket);
            this.groupBoxStatistics.Controls.Add(this.labelStatsOpenSquareBracket);
            this.groupBoxStatistics.Controls.Add(this.labelStatsCloseAngleBracket);
            this.groupBoxStatistics.Controls.Add(this.labelStatsOpenAngleBracket);
            this.groupBoxStatistics.Controls.Add(this.labelStatsMinus);
            this.groupBoxStatistics.Controls.Add(this.labelStatsPlus);
            this.groupBoxStatistics.Controls.Add(this.labelStatsComma);
            this.groupBoxStatistics.Controls.Add(this.labelStatsDot);
            this.groupBoxStatistics.Controls.Add(this.label11);
            this.groupBoxStatistics.Controls.Add(this.label10);
            this.groupBoxStatistics.Controls.Add(this.label9);
            this.groupBoxStatistics.Controls.Add(this.label7);
            this.groupBoxStatistics.Controls.Add(this.label8);
            this.groupBoxStatistics.Controls.Add(this.label5);
            this.groupBoxStatistics.Controls.Add(this.label6);
            this.groupBoxStatistics.Controls.Add(this.label4);
            this.groupBoxStatistics.Controls.Add(this.label3);
            this.groupBoxStatistics.Controls.Add(this.labelExecutionTime);
            this.groupBoxStatistics.Controls.Add(this.label2);
            this.groupBoxStatistics.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxStatistics.Location = new System.Drawing.Point(3, 419);
            this.groupBoxStatistics.Name = "groupBoxStatistics";
            this.groupBoxStatistics.Padding = new System.Windows.Forms.Padding(8);
            this.groupBoxStatistics.Size = new System.Drawing.Size(328, 185);
            this.groupBoxStatistics.TabIndex = 2;
            this.groupBoxStatistics.TabStop = false;
            this.groupBoxStatistics.Text = "Statistics";
            // 
            // labelStatsTotal
            // 
            this.labelStatsTotal.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelStatsTotal.Location = new System.Drawing.Point(59, 165);
            this.labelStatsTotal.Name = "labelStatsTotal";
            this.labelStatsTotal.Size = new System.Drawing.Size(127, 14);
            this.labelStatsTotal.TabIndex = 19;
            this.labelStatsTotal.Text = "-";
            this.labelStatsTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelStatsCloseSquareBracket
            // 
            this.labelStatsCloseSquareBracket.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelStatsCloseSquareBracket.Location = new System.Drawing.Point(59, 150);
            this.labelStatsCloseSquareBracket.Name = "labelStatsCloseSquareBracket";
            this.labelStatsCloseSquareBracket.Size = new System.Drawing.Size(127, 14);
            this.labelStatsCloseSquareBracket.TabIndex = 18;
            this.labelStatsCloseSquareBracket.Text = "-";
            this.labelStatsCloseSquareBracket.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelStatsOpenSquareBracket
            // 
            this.labelStatsOpenSquareBracket.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelStatsOpenSquareBracket.Location = new System.Drawing.Point(59, 135);
            this.labelStatsOpenSquareBracket.Name = "labelStatsOpenSquareBracket";
            this.labelStatsOpenSquareBracket.Size = new System.Drawing.Size(127, 14);
            this.labelStatsOpenSquareBracket.TabIndex = 17;
            this.labelStatsOpenSquareBracket.Text = "-";
            this.labelStatsOpenSquareBracket.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelStatsCloseAngleBracket
            // 
            this.labelStatsCloseAngleBracket.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelStatsCloseAngleBracket.Location = new System.Drawing.Point(59, 120);
            this.labelStatsCloseAngleBracket.Name = "labelStatsCloseAngleBracket";
            this.labelStatsCloseAngleBracket.Size = new System.Drawing.Size(127, 14);
            this.labelStatsCloseAngleBracket.TabIndex = 16;
            this.labelStatsCloseAngleBracket.Text = "-";
            this.labelStatsCloseAngleBracket.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelStatsOpenAngleBracket
            // 
            this.labelStatsOpenAngleBracket.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelStatsOpenAngleBracket.Location = new System.Drawing.Point(59, 105);
            this.labelStatsOpenAngleBracket.Name = "labelStatsOpenAngleBracket";
            this.labelStatsOpenAngleBracket.Size = new System.Drawing.Size(127, 14);
            this.labelStatsOpenAngleBracket.TabIndex = 15;
            this.labelStatsOpenAngleBracket.Text = "-";
            this.labelStatsOpenAngleBracket.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelStatsMinus
            // 
            this.labelStatsMinus.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelStatsMinus.Location = new System.Drawing.Point(59, 90);
            this.labelStatsMinus.Name = "labelStatsMinus";
            this.labelStatsMinus.Size = new System.Drawing.Size(127, 14);
            this.labelStatsMinus.TabIndex = 14;
            this.labelStatsMinus.Text = "-";
            this.labelStatsMinus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelStatsPlus
            // 
            this.labelStatsPlus.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelStatsPlus.Location = new System.Drawing.Point(59, 75);
            this.labelStatsPlus.Name = "labelStatsPlus";
            this.labelStatsPlus.Size = new System.Drawing.Size(127, 14);
            this.labelStatsPlus.TabIndex = 13;
            this.labelStatsPlus.Text = "-";
            this.labelStatsPlus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelStatsComma
            // 
            this.labelStatsComma.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelStatsComma.Location = new System.Drawing.Point(59, 60);
            this.labelStatsComma.Name = "labelStatsComma";
            this.labelStatsComma.Size = new System.Drawing.Size(127, 14);
            this.labelStatsComma.TabIndex = 12;
            this.labelStatsComma.Text = "-";
            this.labelStatsComma.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelStatsDot
            // 
            this.labelStatsDot.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelStatsDot.Location = new System.Drawing.Point(59, 45);
            this.labelStatsDot.Name = "labelStatsDot";
            this.labelStatsDot.Size = new System.Drawing.Size(127, 14);
            this.labelStatsDot.TabIndex = 11;
            this.labelStatsDot.Text = "-";
            this.labelStatsDot.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label11.Location = new System.Drawing.Point(11, 165);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(49, 14);
            this.label11.TabIndex = 10;
            this.label11.Text = "total:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label10.Location = new System.Drawing.Point(11, 150);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 14);
            this.label10.TabIndex = 9;
            this.label10.Text = "] -->";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label9.Location = new System.Drawing.Point(11, 135);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 14);
            this.label9.TabIndex = 8;
            this.label9.Text = "[ -->";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(11, 120);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 14);
            this.label7.TabIndex = 7;
            this.label7.Text = "> -->";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(11, 105);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 14);
            this.label8.TabIndex = 6;
            this.label8.Text = "< -->";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(11, 90);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 14);
            this.label5.TabIndex = 5;
            this.label5.Text = "- -->";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(11, 75);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 14);
            this.label6.TabIndex = 4;
            this.label6.Text = "+ -->";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(11, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 14);
            this.label4.TabIndex = 3;
            this.label4.Text = ", -->";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(11, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 14);
            this.label3.TabIndex = 2;
            this.label3.Text = ". -->";
            // 
            // labelExecutionTime
            // 
            this.labelExecutionTime.AutoSize = true;
            this.labelExecutionTime.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelExecutionTime.Location = new System.Drawing.Point(126, 21);
            this.labelExecutionTime.Name = "labelExecutionTime";
            this.labelExecutionTime.Size = new System.Drawing.Size(14, 14);
            this.labelExecutionTime.TabIndex = 1;
            this.labelExecutionTime.Text = "-";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(11, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 14);
            this.label2.TabIndex = 0;
            this.label2.Text = "Execution time: ";
            // 
            // flowLayoutPanelController
            // 
            this.flowLayoutPanelController.Controls.Add(this.buttonStart);
            this.flowLayoutPanelController.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelController.Location = new System.Drawing.Point(0, 607);
            this.flowLayoutPanelController.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanelController.Name = "flowLayoutPanelController";
            this.flowLayoutPanelController.Size = new System.Drawing.Size(334, 54);
            this.flowLayoutPanelController.TabIndex = 3;
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(3, 3);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(76, 47);
            this.buttonStart.TabIndex = 0;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // comboBoxCodes
            // 
            this.comboBoxCodes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxCodes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCodes.FormattingEnabled = true;
            this.comboBoxCodes.Location = new System.Drawing.Point(3, 3);
            this.comboBoxCodes.Name = "comboBoxCodes";
            this.comboBoxCodes.Size = new System.Drawing.Size(306, 21);
            this.comboBoxCodes.TabIndex = 1;
            this.comboBoxCodes.SelectedIndexChanged += new System.EventHandler(this.comboBoxCodes_SelectedIndexChanged);
            // 
            // tableLayoutPanelCode
            // 
            this.tableLayoutPanelCode.ColumnCount = 1;
            this.tableLayoutPanelCode.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelCode.Controls.Add(this.comboBoxCodes, 0, 0);
            this.tableLayoutPanelCode.Controls.Add(this.textBoxCode, 0, 1);
            this.tableLayoutPanelCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelCode.Location = new System.Drawing.Point(8, 21);
            this.tableLayoutPanelCode.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanelCode.Name = "tableLayoutPanelCode";
            this.tableLayoutPanelCode.RowCount = 2;
            this.tableLayoutPanelCode.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelCode.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelCode.Size = new System.Drawing.Size(312, 296);
            this.tableLayoutPanelCode.TabIndex = 2;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1284, 661);
            this.Controls.Add(this.tableLayoutPanelMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1300, 700);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Brainfuck";
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.tableLayoutPanelConsole.ResumeLayout(false);
            this.tableLayoutPanelConsole.PerformLayout();
            this.groupBoxOutput.ResumeLayout(false);
            this.groupBoxOutput.PerformLayout();
            this.tableLayoutPanelSettings.ResumeLayout(false);
            this.groupBoxProgram.ResumeLayout(false);
            this.groupBoxSettings.ResumeLayout(false);
            this.groupBoxSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMemoryCells)).EndInit();
            this.groupBoxStatistics.ResumeLayout(false);
            this.groupBoxStatistics.PerformLayout();
            this.flowLayoutPanelController.ResumeLayout(false);
            this.tableLayoutPanelCode.ResumeLayout(false);
            this.tableLayoutPanelCode.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private System.Windows.Forms.GroupBox groupBoxOutput;
        private System.Windows.Forms.TextBox textBoxOutput;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelConsole;
        private System.Windows.Forms.TextBox textBoxInput;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelSettings;
        private System.Windows.Forms.GroupBox groupBoxProgram;
        private System.Windows.Forms.TextBox textBoxCode;
        private System.Windows.Forms.GroupBox groupBoxSettings;
        private System.Windows.Forms.CheckBox checkBoxVerbose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDownMemoryCells;
        private System.Windows.Forms.ComboBox comboBoxRuntime;
        private System.Windows.Forms.GroupBox groupBoxStatistics;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelExecutionTime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label labelStatsDot;
        private System.Windows.Forms.Label labelStatsPlus;
        private System.Windows.Forms.Label labelStatsComma;
        private System.Windows.Forms.Label labelStatsMinus;
        private System.Windows.Forms.Label labelStatsTotal;
        private System.Windows.Forms.Label labelStatsCloseSquareBracket;
        private System.Windows.Forms.Label labelStatsOpenSquareBracket;
        private System.Windows.Forms.Label labelStatsCloseAngleBracket;
        private System.Windows.Forms.Label labelStatsOpenAngleBracket;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelController;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.ComboBox comboBoxCodes;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelCode;
    }
}

