﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brainfuck
{
    class RuntimeOptions
    {
        public bool Verbose { get; set; }
        public int MemoryCells { get; set; }
        public Action<string> OutputWrite { get; set; }
        public Func<Task<char>> ReadInput { get; set; }
    }

    class RuntimeStatistics
    {
        public long ExecutionTime { get; set; }
        public long DotOps { get; set; }
        public long CommaOps { get; set; }
        public long PlusOps { get; set; }
        public long MinusOps { get; set; }
        public long OpenAngleBracketOps { get; set; }
        public long CloseAngleBracketOps { get; set; }
        public long OpenSquareBracketOps { get; set; }
        public long CloseSquareBracketOps { get; set; }
        public long TotalOps 
        { 
            get
            {
                return DotOps + CommaOps + PlusOps + MinusOps + OpenAngleBracketOps + CloseAngleBracketOps + OpenSquareBracketOps + CloseSquareBracketOps;
            }
        }
    }

    abstract class IBrainfuckRuntime
    {
        protected RuntimeOptions options = new RuntimeOptions();
        protected RuntimeStatistics stats = new RuntimeStatistics();

        public virtual void SetOptions(RuntimeOptions options)
        {
            this.options = options;
        }

        public virtual RuntimeStatistics GetRuntimeStatistics()
        {
            return stats;
        }

        public abstract Task Execute(string code);
    }
}
