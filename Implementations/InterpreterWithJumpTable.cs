﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Brainfuck.Implementations
{
    class InterpreterWithJumpTable : IBrainfuckRuntime
    {
        public override async Task Execute(string code)
        {
            var stopWatch = new MicroStopwatch();
            stopWatch.Start();

            await MainLoop(code);

            stopWatch.Stop();
            stats.ExecutionTime = stopWatch.ElapsedMicroseconds;
        }

        private async Task MainLoop(string code)
        {
            code = GetCode(code);
            var jumptable = GetJumptable(code);
            if (jumptable == null) return;

            var memory = new byte[options.MemoryCells];
            int dataptr = 0;
            int pc = 0;

            while (pc < code.Length)
            {
                var opcode = code[pc];

                switch (opcode)
                {
                    case '<':
                        {
                            if (options.Verbose) stats.OpenAngleBracketOps += 1;

                            dataptr -= 1;

                            if (dataptr < 0)
                            {
                                dataptr = options.MemoryCells - 1;
                            }

                            break;
                        }
                    case '>':
                        {
                            if (options.Verbose) stats.CloseAngleBracketOps += 1;

                            dataptr += 1;

                            if (dataptr >= options.MemoryCells)
                            {
                                dataptr = 0;
                            }

                            break;
                        }
                    case '+':
                        {
                            if (options.Verbose) stats.PlusOps += 1;

                            memory[dataptr] += 1;

                            break;
                        }
                    case '-':
                        {
                            if (options.Verbose) stats.MinusOps += 1;

                            memory[dataptr] -= 1;

                            break;
                        }
                    case '.':
                        {
                            if (options.Verbose) stats.DotOps += 1;

                            options.OutputWrite(Convert.ToChar(memory[dataptr]).ToString());

                            break;
                        }
                    case ',':
                        {
                            if (options.Verbose) stats.CommaOps += 1;

                            memory[dataptr] = Convert.ToByte(await options.ReadInput());

                            break;
                        }
                    case '[':
                        {
                            if (options.Verbose) stats.OpenSquareBracketOps += 1;

                            if (memory[dataptr] == 0)
                            {
                                pc = jumptable[pc];
                            }

                            break;
                        }
                    case ']':
                        {
                            if (options.Verbose) stats.CloseSquareBracketOps += 1;

                            if (memory[dataptr] != 0)
                            {
                                pc = jumptable[pc];
                            }

                            break;
                        }
                }

                pc += 1;
            }
        }

        int[] GetJumptable(string code)
        {
            var pc = 0;
            var jumptable = new int[code.Length];

            while (pc < code.Length)
            {
                var opcode = code[pc];

                if (opcode == '[')
                {
                    var bracketNesting = 1;
                    var seek = pc;

                    while (bracketNesting > 0 && ++seek < code.Length)
                    {
                        if (code[seek] == ']')
                        {
                            bracketNesting -= 1;
                        }
                        else if (code[seek] == '[')
                        {
                            bracketNesting += 1;
                        }
                    }

                    if (bracketNesting == 0)
                    {
                        jumptable[pc] = seek;
                        jumptable[seek] = pc;
                    } 
                    else
                    {
                        options.OutputWrite($"error: unmatched '[' at pc = {pc}");

                        return null;
                    }
                }

                pc += 1;
            }

            return jumptable;
        }

        string GetCode(string code)
        {
            var validChars = new char[] { '>', '<', '+', '-', '.', ',', '[', ']' };

            var output = new StringBuilder();

            foreach (var ch in code)
            {
                if (validChars.Contains(ch))
                {
                    output.Append(ch);
                }
            }

            return output.ToString();
        }
    }
}
