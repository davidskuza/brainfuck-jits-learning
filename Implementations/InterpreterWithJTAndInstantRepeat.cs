﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Brainfuck.Implementations
{
    enum BfOpCode
    {
        NOP,
        INC_PTR,
        DEC_PTR,
        INC_DATA,
        DEC_DATA,
        READ_INPUT,
        WRITE_OUTPUT,
        JMP_IF_ZERO,
        JMP_IF_NOT_ZERO
    }

    class BfOp
    {
        public BfOpCode OpCode { get; set; }
        public int Argument { get; set; }
    }

    class InterpreterWithJTAndInstantRepeat : IBrainfuckRuntime
    {
        public override async Task Execute(string code)
        {
            var stopWatch = new MicroStopwatch();
            stopWatch.Start();

            await MainLoop(code);

            stopWatch.Stop();
            stats.ExecutionTime = stopWatch.ElapsedMicroseconds;
        }

        private async Task MainLoop(string code)
        {
            code = GetCode(code);
            var opcodes = GetOpcodes(code);
            if (opcodes == null) return;

            var memory = new byte[options.MemoryCells];
            int dataptr = 0;
            int pc = 0;

            while (pc < opcodes.Length)
            {
                var op = opcodes[pc];

                switch (op.OpCode)
                {
                    case BfOpCode.DEC_PTR:
                        {
                            if (options.Verbose) stats.OpenAngleBracketOps += 1;

                            dataptr -= op.Argument;

                            if (dataptr < 0)
                            {
                                dataptr = options.MemoryCells - 1;
                            }

                            break;
                        }
                    case BfOpCode.INC_PTR:
                        {
                            if (options.Verbose) stats.CloseAngleBracketOps += 1;

                            dataptr += op.Argument;

                            if (dataptr >= options.MemoryCells)
                            {
                                dataptr = 0;
                            }

                            break;
                        }
                    case BfOpCode.INC_DATA:
                        {
                            if (options.Verbose) stats.PlusOps += 1;

                            memory[dataptr] += (byte)op.Argument;

                            break;
                        }
                    case BfOpCode.DEC_DATA:
                        {
                            if (options.Verbose) stats.MinusOps += 1;

                            memory[dataptr] -= (byte)op.Argument;

                            break;
                        }
                    case BfOpCode.WRITE_OUTPUT:
                        {
                            if (options.Verbose) stats.DotOps += 1;

                            for (var i = 0; i < op.Argument; ++i)
                            {
                                options.OutputWrite(Convert.ToChar(memory[dataptr]).ToString());
                            }

                            break;
                        }
                    case BfOpCode.READ_INPUT:
                        {
                            if (options.Verbose) stats.CommaOps += 1;

                            for (var i = 0; i < op.Argument; ++i)
                            {
                                memory[dataptr] = Convert.ToByte(await options.ReadInput());
                            }

                            break;
                        }
                    case BfOpCode.JMP_IF_ZERO:
                        {
                            if (options.Verbose) stats.OpenSquareBracketOps += 1;

                            if (memory[dataptr] == 0)
                            {
                                pc = op.Argument;
                            }

                            break;
                        }
                    case BfOpCode.JMP_IF_NOT_ZERO:
                        {
                            if (options.Verbose) stats.CloseSquareBracketOps += 1;

                            if (memory[dataptr] != 0)
                            {
                                pc = op.Argument;
                            }

                            break;
                        }
                }

                pc += 1;
            }
        }

        BfOp[] GetOpcodes(string code)
        {
            var opcodes = new List<BfOp>();

            var pc = 0;

            var openBracketStack = new Stack<int>();

            while (pc < code.Length)
            {
                var opcode = code[pc];

                if (opcode == '[')
                {
                    openBracketStack.Push(opcodes.Count);
                    opcodes.Add(new BfOp
                    {
                        OpCode = BfOpCode.JMP_IF_ZERO
                    });

                    pc += 1;
                }
                else if (opcode == ']')
                {
                    if (openBracketStack.Count == 0)
                    {
                        options.OutputWrite($"error: unmatched closing ']' at pc = {pc}");

                        return null;
                    }

                    var openBracketOffset = openBracketStack.Pop();

                    opcodes[openBracketOffset].Argument = opcodes.Count;
                    opcodes.Add(new BfOp
                    {
                        OpCode = BfOpCode.JMP_IF_NOT_ZERO,
                        Argument = openBracketOffset
                    });

                    pc += 1;
                }
                else
                {
                    var start = pc++;

                    while (pc < code.Length && code[pc] == opcode)
                    {
                        pc += 1;
                    }

                    var bfOp = new BfOp
                    {
                        Argument = pc - start
                    };

                    switch (opcode)
                    {
                        case '<': bfOp.OpCode = BfOpCode.DEC_PTR; break;
                        case '>': bfOp.OpCode = BfOpCode.INC_PTR; break;
                        case '+': bfOp.OpCode = BfOpCode.INC_DATA; break;
                        case '-': bfOp.OpCode = BfOpCode.DEC_DATA; break;
                        case '.': bfOp.OpCode = BfOpCode.WRITE_OUTPUT; break;
                        case ',': bfOp.OpCode = BfOpCode.READ_INPUT; break;
                    }

                    opcodes.Add(bfOp);
                }
            }

            return opcodes.ToArray();
        }

        string GetCode(string code)
        {
            var validChars = new char[] { '>', '<', '+', '-', '.', ',', '[', ']' };

            var output = new StringBuilder();

            foreach (var ch in code)
            {
                if (validChars.Contains(ch))
                {
                    output.Append(ch);
                }
            }

            return output.ToString();
        }
    }
}
