﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace Brainfuck.Implementations
{
    class JitNetSimple : IBrainfuckRuntime
    {
        delegate void JittedCode(byte[] memory);

        static Dictionary<string, JittedCode> _jit_cache = new Dictionary<string, JittedCode>();

        public override async Task Execute(string code)
        {
            var stopWatch = new MicroStopwatch();
            stopWatch.Start();

            var memory = new byte[options.MemoryCells];

            if (!_jit_cache.ContainsKey(code))
            {
                _jit_cache[code] = JITCode(code);
            }

            _jit_cache[code](memory);

            stopWatch.Stop();
            stats.ExecutionTime = stopWatch.ElapsedMicroseconds;
        }

        JittedCode JITCode(string code)
        {
            code = GetCode(code);

            var dynamicMethod = new DynamicMethod("__JittedCode", typeof(void), new Type[] { typeof(JitNetSimple), typeof(byte[]) }, typeof(JitNetSimple));

            //{
            //    var assemblyName = new AssemblyName("JitNetSimple");
            //    var assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Save, Environment.CurrentDirectory);
            //    var moduleBuilder = assemblyBuilder.DefineDynamicModule(assemblyName.Name, $"{assemblyName.Name}.dll", true);

            //    var builder = moduleBuilder.DefineType("Test", TypeAttributes.Public);
            //    var methodBuilder = builder.DefineMethod("__JittedCode", MethodAttributes.Public, typeof(void), new Type[] { typeof(byte[]) });

            //    EmitCode(methodBuilder.GetILGenerator(), code);

            //    var t = builder.CreateType();
            //    assemblyBuilder.Save($"{assemblyName.Name}.dll");

            //    Environment.Exit(0);
            //}

            EmitCode(dynamicMethod.GetILGenerator(), code);

            return dynamicMethod.CreateDelegate(typeof(JittedCode), this) as JittedCode;
        }

        void Tester(byte[] memory)
        {
            var dataptr = 0;

            dataptr += 1;
            dataptr += 1;
            memory[dataptr] += 1;

            options.OutputWrite(Convert.ToChar(memory[dataptr]).ToString());

            memory[dataptr] = Convert.ToByte(options.ReadInput().Result);

            if (memory[dataptr] == 0)
            {

            }

            if (memory[dataptr] != 0)
            {

            }

            memory[dataptr] = 0;

            var count = 123;

            var sb = new StringBuilder(count);
            sb.Append(Convert.ToChar(memory[dataptr]), count);
            options.OutputWrite(sb.ToString());
        }

        void EmitCode(ILGenerator gen, string code)
        {
            var fi_options = typeof(JitNetSimple).GetField("options", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            var mi_get_OutputWrite = typeof(RuntimeOptions).GetProperty("OutputWrite").GetGetMethod();
            var mi_get_ReadInput = typeof(RuntimeOptions).GetProperty("ReadInput").GetGetMethod();

            var mi_toChar = typeof(Convert).GetMethod("ToChar", new Type[] { typeof(byte) });
            var mi_char_ToString = typeof(Char).GetMethod("ToString", Type.EmptyTypes);

            var mi_action_string_invoke = typeof(Action<string>).GetMethod("Invoke", new Type[] { typeof(string) });
            var mi_func_task_char_invoke = typeof(Func<Task<char>>).GetMethod("Invoke", Type.EmptyTypes);
            var mi_task_char_get_Result = typeof(Task<char>).GetProperty("Result").GetGetMethod();

            var mi_toByte = typeof(Convert).GetMethod("ToByte", new Type[] { typeof(char) });

            //var mi_writeOutput = (typeof(RuntimeOptions).GetProperty("OutputWrite").GetValue(this.options) as Action<string>).Method;
            //var mi_readInput = (typeof(RuntimeOptions).GetProperty("ReadInput").GetValue(this.options) as Func<Task<char>>).Method;

            gen.DeclareLocal(typeof(int)); // dataptr
            gen.DeclareLocal(typeof(char)); // for OutputWrite

            // dataptr = 0;
            //gen.Emit(OpCodes.Ldc_I4_0);
            //gen.Emit(OpCodes.Stloc_0);

            Stack<Label> openBracketLabels = new Stack<Label>();
            Stack<Label> closeBracketLabels = new Stack<Label>();

            int pc = 0;
            while (pc < code.Length)
            {
                var opcode = code[pc];

                switch (opcode)
                {
                    case '<':
                        {
                            // dataptr -= 1;
                            gen.Emit(OpCodes.Ldloc_0);
                            gen.Emit(OpCodes.Ldc_I4_1);
                            gen.Emit(OpCodes.Sub);
                            gen.Emit(OpCodes.Stloc_0);

                            var condLabel = gen.DefineLabel();

                            gen.Emit(OpCodes.Ldloc_0);
                            gen.Emit(OpCodes.Ldc_I4_0);
                            gen.Emit(OpCodes.Bge_S, condLabel);

                            gen.Emit(OpCodes.Ldarg_1);
                            gen.Emit(OpCodes.Ldlen);
                            gen.Emit(OpCodes.Conv_I4);
                            gen.Emit(OpCodes.Ldc_I4_1);
                            gen.Emit(OpCodes.Sub);
                            gen.Emit(OpCodes.Stloc_0);

                            // faster, but compiled from external data and argument
                            //gen.Emit(OpCodes.Ldc_I4, options.MemoryCells - 1);
                            //gen.Emit(OpCodes.Stloc_0);

                            gen.MarkLabel(condLabel);

                            break;
                        }
                    case '>':
                        {
                            // dataptr += 1;
                            gen.Emit(OpCodes.Ldloc_0);
                            gen.Emit(OpCodes.Ldc_I4_1);
                            gen.Emit(OpCodes.Add);
                            gen.Emit(OpCodes.Stloc_0);

                            var condLabel = gen.DefineLabel();

                            gen.Emit(OpCodes.Ldloc_0);

                            gen.Emit(OpCodes.Ldarg_1);
                            gen.Emit(OpCodes.Ldlen);
                            gen.Emit(OpCodes.Conv_I4);
                            //gen.Emit(OpCodes.Ldc_I4, options.MemoryCells);

                            gen.Emit(OpCodes.Blt_S, condLabel);

                            gen.Emit(OpCodes.Ldc_I4_0);
                            gen.Emit(OpCodes.Stloc_0);

                            gen.MarkLabel(condLabel);

                            break;
                        }
                    case '+':
                        {
                            gen.Emit(OpCodes.Ldarg_1);
                            gen.Emit(OpCodes.Ldloc_0);
                            gen.Emit(OpCodes.Ldelema, typeof(byte));
                            gen.Emit(OpCodes.Dup);
                            gen.Emit(OpCodes.Ldind_U1);
                            gen.Emit(OpCodes.Ldc_I4_1);
                            gen.Emit(OpCodes.Add);
                            gen.Emit(OpCodes.Conv_U1);
                            gen.Emit(OpCodes.Stind_I1);

                            break;
                        }
                    case '-':
                        {
                            gen.Emit(OpCodes.Ldarg_1);
                            gen.Emit(OpCodes.Ldloc_0);
                            gen.Emit(OpCodes.Ldelema, typeof(byte));
                            gen.Emit(OpCodes.Dup);
                            gen.Emit(OpCodes.Ldind_U1);
                            gen.Emit(OpCodes.Ldc_I4_1);
                            gen.Emit(OpCodes.Sub);
                            gen.Emit(OpCodes.Conv_U1);
                            gen.Emit(OpCodes.Stind_I1);

                            break;
                        }
                    case '.':
                        {
                            gen.Emit(OpCodes.Ldarg_0);
                            gen.Emit(OpCodes.Ldfld, fi_options);
                            gen.Emit(OpCodes.Callvirt, mi_get_OutputWrite);
                            gen.Emit(OpCodes.Ldarg_1);
                            gen.Emit(OpCodes.Ldloc_0);
                            gen.Emit(OpCodes.Ldelem_U1);
                            gen.Emit(OpCodes.Call, mi_toChar);
                            gen.Emit(OpCodes.Stloc_1);
                            gen.Emit(OpCodes.Ldloca_S, 1);
                            gen.Emit(OpCodes.Call, mi_char_ToString);
                            gen.Emit(OpCodes.Callvirt, mi_action_string_invoke);

                            break;
                        }
                    case ',':
                        {
                            gen.Emit(OpCodes.Ldarg_1);
                            gen.Emit(OpCodes.Ldloc_0);

                            gen.Emit(OpCodes.Ldarg_0);
                            gen.Emit(OpCodes.Ldfld, fi_options);
                            gen.Emit(OpCodes.Callvirt, mi_get_ReadInput);
                            gen.Emit(OpCodes.Callvirt, mi_func_task_char_invoke);
                            gen.Emit(OpCodes.Callvirt, mi_task_char_get_Result);

                            gen.Emit(OpCodes.Call, mi_toByte);

                            gen.Emit(OpCodes.Stelem_I1);

                            break;
                        }
                    case '[':
                        {
                            var condLabel = gen.DefineLabel();

                            gen.Emit(OpCodes.Ldarg_1);
                            gen.Emit(OpCodes.Ldloc_0);
                            gen.Emit(OpCodes.Ldelem_U1);
                            gen.Emit(OpCodes.Ldc_I4_0);
                            gen.Emit(OpCodes.Bne_Un_S, condLabel);

                            var matchingBracketLabel = gen.DefineLabel();
                            gen.Emit(OpCodes.Br, matchingBracketLabel);
                            closeBracketLabels.Push(matchingBracketLabel);

                            gen.MarkLabel(condLabel);

                            openBracketLabels.Push(condLabel); // reuse the same label

                            break;
                        }
                    case ']':
                        {
                            var condLabel = closeBracketLabels.Pop();

                            gen.Emit(OpCodes.Ldarg_1);
                            gen.Emit(OpCodes.Ldloc_0);
                            gen.Emit(OpCodes.Ldelem_U1);
                            gen.Emit(OpCodes.Ldc_I4_0);
                            gen.Emit(OpCodes.Beq_S, condLabel);

                            gen.Emit(OpCodes.Br, openBracketLabels.Pop());

                            gen.MarkLabel(condLabel); // above condition and '[' jumps here

                            break;
                        }
                }

                pc += 1;
            }

            gen.Emit(OpCodes.Ret);
        }

        string GetCode(string code)
        {
            var validChars = new char[] { '>', '<', '+', '-', '.', ',', '[', ']' };

            var output = new StringBuilder();

            foreach (var ch in code)
            {
                if (validChars.Contains(ch))
                {
                    output.Append(ch);
                }
            }

            return output.ToString();
        }
    }
}
