﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace Brainfuck.Implementations
{
    class JitNetWithRepeatsCount : IBrainfuckRuntime
    {
        delegate void JittedCode(byte[] memory);

        static Dictionary<string, JittedCode> _jit_cache = new Dictionary<string, JittedCode>();

        public override async Task Execute(string code)
        {
            var stopWatch = new MicroStopwatch();
            stopWatch.Start();

            var memory = new byte[options.MemoryCells];

            if (!_jit_cache.ContainsKey(code))
            {
                _jit_cache[code] = JITCode(code);
            }

            _jit_cache[code](memory);

            stopWatch.Stop();
            stats.ExecutionTime = stopWatch.ElapsedMicroseconds;
        }

        JittedCode JITCode(string code)
        {
            code = GetCode(code);

            var dynamicMethod = new DynamicMethod("__JittedCode", typeof(void), new Type[] { typeof(JitNetWithRepeatsCount), typeof(byte[]) }, typeof(JitNetWithRepeatsCount));

            //{
            //    var assemblyName = new AssemblyName("JitNetWithRepeatsCount");
            //    var assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Save, Environment.CurrentDirectory);
            //    var moduleBuilder = assemblyBuilder.DefineDynamicModule(assemblyName.Name, $"{assemblyName.Name}.dll", true);

            //    var builder = moduleBuilder.DefineType("Test", TypeAttributes.Public);
            //    var methodBuilder = builder.DefineMethod("__JittedCode", MethodAttributes.Public, typeof(void), new Type[] { typeof(byte[]) });

            //    EmitCode(methodBuilder.GetILGenerator(), code);

            //    var t = builder.CreateType();
            //    assemblyBuilder.Save($"{assemblyName.Name}.dll");

            //    Environment.Exit(0);
            //}

            EmitCode(dynamicMethod.GetILGenerator(), code);

            return dynamicMethod.CreateDelegate(typeof(JittedCode), this) as JittedCode;
        }

        void EmitCode(ILGenerator gen, string code)
        {
            var fi_options = typeof(JitNetWithRepeatsCount).GetField("options", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            var mi_get_OutputWrite = typeof(RuntimeOptions).GetProperty("OutputWrite").GetGetMethod();
            var mi_get_ReadInput = typeof(RuntimeOptions).GetProperty("ReadInput").GetGetMethod();

            var mi_toChar = typeof(Convert).GetMethod("ToChar", new Type[] { typeof(byte) });
            var mi_char_ToString = typeof(Char).GetMethod("ToString", Type.EmptyTypes);

            var mi_action_string_invoke = typeof(Action<string>).GetMethod("Invoke", new Type[] { typeof(string) });
            var mi_func_task_char_invoke = typeof(Func<Task<char>>).GetMethod("Invoke", Type.EmptyTypes);
            var mi_task_char_get_Result = typeof(Task<char>).GetProperty("Result").GetGetMethod();

            var mi_toByte = typeof(Convert).GetMethod("ToByte", new Type[] { typeof(char) });

            var ci_stringBuilder_capacity = typeof(StringBuilder).GetConstructor(new Type[] { typeof(int) });
            var mi_stringBuilder_append_char_repeat = typeof(StringBuilder).GetMethod("Append", new Type[] { typeof(char), typeof(int) });

            var mi_object_ToString = typeof(object).GetMethod("ToString", Type.EmptyTypes);

            gen.DeclareLocal(typeof(int));              // dataptr
            gen.DeclareLocal(typeof(char));             // for OutputWrite
            gen.DeclareLocal(typeof(StringBuilder));    // for output multiple

            Stack<Label> openBracketLabels = new Stack<Label>();
            Stack<Label> closeBracketLabels = new Stack<Label>();

            int pc = 0;
            while (pc < code.Length)
            {
                var opcode = code[pc];

                if (opcode == '[')
                {
                    var condLabel = gen.DefineLabel();

                    gen.Emit(OpCodes.Ldarg_1);
                    gen.Emit(OpCodes.Ldloc_0);
                    gen.Emit(OpCodes.Ldelem_U1);
                    gen.Emit(OpCodes.Ldc_I4_0);
                    gen.Emit(OpCodes.Bne_Un_S, condLabel);

                    var matchingBracketLabel = gen.DefineLabel();
                    gen.Emit(OpCodes.Br, matchingBracketLabel);
                    closeBracketLabels.Push(matchingBracketLabel);

                    gen.MarkLabel(condLabel);

                    openBracketLabels.Push(condLabel); // reuse the same label

                    pc += 1;
                }
                else if (opcode == ']')
                {
                    var condLabel = closeBracketLabels.Pop();

                    gen.Emit(OpCodes.Ldarg_1);
                    gen.Emit(OpCodes.Ldloc_0);
                    gen.Emit(OpCodes.Ldelem_U1);
                    gen.Emit(OpCodes.Ldc_I4_0);
                    gen.Emit(OpCodes.Beq_S, condLabel);

                    gen.Emit(OpCodes.Br, openBracketLabels.Pop());

                    gen.MarkLabel(condLabel); // above condition and '[' jumps here

                    pc += 1;
                }
                else
                {
                    var start = pc++;

                    while (pc < code.Length && code[pc] == opcode)
                    {
                        pc += 1;
                    }

                    var count = pc - start;

                    switch (opcode)
                    {
                        case '<':
                            {
                                // dataptr -= count;
                                gen.Emit(OpCodes.Ldloc_0);
                                EmitLdInt(gen, count);
                                gen.Emit(OpCodes.Sub);
                                gen.Emit(OpCodes.Stloc_0);

                                var condLabel = gen.DefineLabel();

                                gen.Emit(OpCodes.Ldloc_0);
                                gen.Emit(OpCodes.Ldc_I4_0);
                                gen.Emit(OpCodes.Bge_S, condLabel);

                                gen.Emit(OpCodes.Ldarg_1);
                                gen.Emit(OpCodes.Ldlen);
                                gen.Emit(OpCodes.Conv_I4);
                                gen.Emit(OpCodes.Ldc_I4_1);
                                gen.Emit(OpCodes.Sub);
                                gen.Emit(OpCodes.Stloc_0);

                                gen.MarkLabel(condLabel);

                                break;
                            }
                        case '>':
                            {
                                // dataptr += count;
                                gen.Emit(OpCodes.Ldloc_0);
                                EmitLdInt(gen, count);
                                gen.Emit(OpCodes.Add);
                                gen.Emit(OpCodes.Stloc_0);

                                var condLabel = gen.DefineLabel();

                                gen.Emit(OpCodes.Ldloc_0);

                                gen.Emit(OpCodes.Ldarg_1);
                                gen.Emit(OpCodes.Ldlen);
                                gen.Emit(OpCodes.Conv_I4);

                                gen.Emit(OpCodes.Blt_S, condLabel);

                                gen.Emit(OpCodes.Ldc_I4_0);
                                gen.Emit(OpCodes.Stloc_0);

                                gen.MarkLabel(condLabel);

                                break;
                            }
                        case '+':
                            {
                                gen.Emit(OpCodes.Ldarg_1);
                                gen.Emit(OpCodes.Ldloc_0);
                                gen.Emit(OpCodes.Ldelema, typeof(byte));
                                gen.Emit(OpCodes.Dup);
                                gen.Emit(OpCodes.Ldind_U1);
                                EmitLdInt(gen, count);
                                gen.Emit(OpCodes.Add);
                                gen.Emit(OpCodes.Conv_U1);
                                gen.Emit(OpCodes.Stind_I1);

                                break;
                            }
                        case '-':
                            {
                                gen.Emit(OpCodes.Ldarg_1);
                                gen.Emit(OpCodes.Ldloc_0);
                                gen.Emit(OpCodes.Ldelema, typeof(byte));
                                gen.Emit(OpCodes.Dup);
                                gen.Emit(OpCodes.Ldind_U1);
                                EmitLdInt(gen, count);
                                gen.Emit(OpCodes.Sub);
                                gen.Emit(OpCodes.Conv_U1);
                                gen.Emit(OpCodes.Stind_I1);

                                break;
                            }
                        case '.':
                            {
                                if (count == 1)
                                {
                                    gen.Emit(OpCodes.Ldarg_0);
                                    gen.Emit(OpCodes.Ldfld, fi_options);
                                    gen.Emit(OpCodes.Callvirt, mi_get_OutputWrite);

                                    gen.Emit(OpCodes.Ldarg_1);
                                    gen.Emit(OpCodes.Ldloc_0);
                                    gen.Emit(OpCodes.Ldelem_U1);
                                    gen.Emit(OpCodes.Call, mi_toChar);
                                    gen.Emit(OpCodes.Stloc_1);
                                    gen.Emit(OpCodes.Ldloca_S, 1);
                                    gen.Emit(OpCodes.Call, mi_char_ToString);
                                    gen.Emit(OpCodes.Callvirt, mi_action_string_invoke);

                                }
                                else
                                {
                                    EmitLdInt(gen, count);
                                    gen.Emit(OpCodes.Newobj, ci_stringBuilder_capacity);
                                    gen.Emit(OpCodes.Stloc_2);
                                    gen.Emit(OpCodes.Ldloc_2);
                                    gen.Emit(OpCodes.Ldarg_1); // memory
                                    gen.Emit(OpCodes.Ldloc_0); // dataptr
                                    gen.Emit(OpCodes.Ldelem_U1);
                                    gen.Emit(OpCodes.Call, mi_toChar);
                                    EmitLdInt(gen, count);
                                    gen.Emit(OpCodes.Callvirt, mi_stringBuilder_append_char_repeat);
                                    gen.Emit(OpCodes.Pop);

                                    gen.Emit(OpCodes.Ldarg_0);
                                    gen.Emit(OpCodes.Ldfld, fi_options);
                                    gen.Emit(OpCodes.Callvirt, mi_get_OutputWrite);
                                    gen.Emit(OpCodes.Ldloc_2);
                                    gen.Emit(OpCodes.Callvirt, mi_object_ToString);
                                    gen.Emit(OpCodes.Callvirt, mi_action_string_invoke);
                                }

                                break;
                            }
                        case ',':
                            {
                                // consecutive calls of this operation doesn't make sense (only last will be remebered)
                                //      so we do not care about optimizations, there is no reason to call it more than once
                                //      without another operations in between
                                for (var i = 0; i < count; ++i)
                                {
                                    gen.Emit(OpCodes.Ldarg_1);
                                    gen.Emit(OpCodes.Ldloc_0);

                                    gen.Emit(OpCodes.Ldarg_0);
                                    gen.Emit(OpCodes.Ldfld, fi_options);
                                    gen.Emit(OpCodes.Callvirt, mi_get_ReadInput);
                                    gen.Emit(OpCodes.Callvirt, mi_func_task_char_invoke);
                                    gen.Emit(OpCodes.Callvirt, mi_task_char_get_Result);

                                    gen.Emit(OpCodes.Call, mi_toByte);

                                    gen.Emit(OpCodes.Stelem_I1);
                                }

                                break;
                            }
                    }
                }
            }

            gen.Emit(OpCodes.Ret);
        }

        private void EmitLdInt(ILGenerator gen, int count)
        {
            if (count == 1)
            {
                gen.Emit(OpCodes.Ldc_I4_1);
            }
            else if (count == 2)
            {
                gen.Emit(OpCodes.Ldc_I4_2);
            }
            else if (count == 3)
            {
                gen.Emit(OpCodes.Ldc_I4_3);
            }
            else if (count == 4)
            {
                gen.Emit(OpCodes.Ldc_I4_4);
            }
            else if (count == 5)
            {
                gen.Emit(OpCodes.Ldc_I4_5);
            }
            else if (count == 6)
            {
                gen.Emit(OpCodes.Ldc_I4_6);
            }
            else if (count == 7)
            {
                gen.Emit(OpCodes.Ldc_I4_7);
            }
            else if (count == 8)
            {
                gen.Emit(OpCodes.Ldc_I4_8);
            }
            else if (count < 128)
            {
                gen.Emit(OpCodes.Ldc_I4_S, count);
            }
            else
            {
                gen.Emit(OpCodes.Ldc_I4, count);
            }
        }

        string GetCode(string code)
        {
            var validChars = new char[] { '>', '<', '+', '-', '.', ',', '[', ']' };

            var output = new StringBuilder();

            foreach (var ch in code)
            {
                if (validChars.Contains(ch))
                {
                    output.Append(ch);
                }
            }

            return output.ToString();
        }
    }
}
